package prak5;

public class Latihan{
    public static void main(String[] args){
        Jurusan TI = new Jurusan();
        TI.setJurusan("Teknik Informatika");
        Jurusan MI = new Jurusan();
        MI.setJurusan("Manajemen Informatika");
        Jurusan TK = new Jurusan();
        TK.setJurusan("Teknik Informatika");
        System.out.println();

        Dosen Budiyanto = new Dosen();
        Budiyanto.setDosen("Budiyanto",TI);
        Budiyanto.cetakDosen();
        System.out.println();

        Dosen Rudiyanto = new Dosen();
        Rudiyanto.setDosen("Rudiyanto",TI);
        Rudiyanto.cetakDosen();
        System.out.println();

        Dosen Sudiyanto = new Dosen();
        Sudiyanto.setDosen("Sudiyanto",TI);
        Sudiyanto.cetakDosen();
        System.out.println();

        MataKuliah keamananSistem = new MataKuliah();
        keamananSistem.setMatkul("Keamanan Sistem", Budiyanto);
        MataKuliah basisdata = new MataKuliah();
        basisdata.setMatkul("Basis Data", Rudiyanto);
        MataKuliah pancasila = new MataKuliah();
        pancasila.setMatkul("Pancasila", Sudiyanto);
        keamananSistem.cetakMatkul();
        basisdata.cetakMatkul();
        pancasila.cetakMatkul();
        System.out.println();

        Mahasiswa Bayu = new Mahasiswa();
        MataKuliah[] pilihBayu = new MataKuliah[3];
        pilihBayu[0] = keamananSistem;
        pilihBayu[1] = basisdata;
        pilihBayu[2] = pancasila;
        Bayu.setMahasiswa("Baju Aji", 123123, pilihBayu);
        Bayu.cetakMhs();

    }
}
