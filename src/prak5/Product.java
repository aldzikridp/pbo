package prak5;

public class Product{
    private String produk;
    private int hgproduk;
    public Product(){
        produk = "-";
    }
    public void setProduct(String produkIn, int hgprodukIn){
        produk = produkIn;
        hgproduk = hgprodukIn;
    }
    public String getProduct(){
        return produk;
    }
    public int getHgProduct(){
        return hgproduk;
    }
    public void cetakProduct(){
        System.out.println("Nama Produk: "+produk);
        System.out.println("Harga Produk: "+hgproduk);
    }
}
