package prak5;

public class Dosen {
    private String nama;
    private Jurusan jurusan = new Jurusan();

    public Dosen(){
        this.nama = "-";
    }
    public String getJurusanDosen() {
        return (jurusan.getJurusan());
    }

    public String getNamaDosen() { return this.nama; }

    public void setDosen(String nama, Jurusan inJurusan) {
        this.nama = nama;
        jurusan = inJurusan;
    }

    public void cetakDosen() {
        System.out.println();
        System.out.println("Nama Dosen:" + this.nama);
        System.out.println("Jurusan:" + getJurusanDosen());
    }
}
