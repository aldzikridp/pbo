package prak5;

public class TransaksiJual{
    private Pembeli tpembeli = new Pembeli();
    private Product[] tproduk = new Product[3];

    public TransaksiJual(){
        tpembeli.setPembeli("belum dimasukkan");
    }

    public void setTransaksi(Pembeli tPembeliIn, Product[] tprodukIn){
        tpembeli=tPembeliIn;
        tproduk=tprodukIn;
    }

    public void cetakTransaksi(){
        int total = 0;
        System.out.println("Nama Pembeli: "+tpembeli.getPembeli());
        System.out.println("Barang yang dibeli:");
        for(int i = 0; i<3; i++){
            System.out.println(tproduk[i].getProduct()+" harga: "+tproduk[i].getHgProduct());
            total += tproduk[i].getHgProduct();
        }
        System.out.println("Total :"+total);
    }
}
