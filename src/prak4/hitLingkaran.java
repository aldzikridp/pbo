package prak4;
class Lingkaran{
	private double r = 4;
	public double luas(){
		return(3.14*r*r);
	}
	public double keliling(){
		return(3.14*2*r);
	}
}
public class hitLingkaran{
	public static void main(String[]args){
		Lingkaran coba = new Lingkaran();
		System.out.println("Luas Lingkaran = "+coba.luas());
		System.out.println("Keliling Lingkaran = "+coba.keliling());
	}
}
