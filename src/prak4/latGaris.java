package prak4;

public class latGaris {
    public static void main(String[] args) {
        Garis a = new Garis();
        Garis b = new Garis(2, 3, 6, 5);
        Garis c = new Garis(2.5, 3, 6, 5);
        Garis d = new Garis(2, 5.3, 6, 5);
        Garis e = new Garis("4", 3, 6, 5);
        Garis f = new Garis(4, 5, 6, "8");
        int[] tkAwalB = b.getAwal();
        int[] tkAkhirB = b.getAkhir();
        System.out.println("Titik awal ("+tkAwalB[0]+","+tkAwalB[1]+")");
        System.out.println("Titik akhir ("+tkAkhirB[0]+","+tkAwalB[1]+")");
    }
}
