package prak4;
class Lingkaran1{
	private double r;

	public double luas(){
		return(3.14*r*r);
	}
	public double keliling(){
		return(3.14*2*r);
	}
	public Lingkaran1(){
		r = 2.5;
	}
	public Lingkaran1(double r_){
		this.r = r_;
	}
	public Lingkaran1(int r_){
		this.r = r_;
	}
	public Lingkaran1(String r_){
		this.r = Integer.parseInt(r_);
	}
	public Lingkaran1(Lingkaran1 li){
		r=li.r;
	}
	public Lingkaran1(Lingkaran1 li, int i){
		r=li.r*2;
	}
	public Lingkaran1(int i, Lingkaran1 li){
		r=li.r;
	}
}
public class hitLingkaran1{
	public static void main(String[]args){
		Lingkaran1 lingA = new Lingkaran1();
		System.out.println("Luas Lingkaran1 = "+lingA.luas());
		System.out.println("Keliling Lingkaran1 = "+lingA.keliling());

		Lingkaran1 lingB = new Lingkaran1(1.2);
		System.out.println("Luas Lingkaran2 = "+lingB.luas());
		System.out.println("Keliling Lingkaran2 = "+lingB.keliling());

		Lingkaran1 lingC = new Lingkaran1(lingB);
		System.out.println("Luas Lingkaran3 = "+lingC.luas());
		System.out.println("Keliling Lingkaran3 = "+lingC.keliling());

		Lingkaran1 lingD = new Lingkaran1(2);
		System.out.println("Luas Lingkaran4 = "+lingD.luas());
		System.out.println("Keliling Lingkaran4 = "+lingD.keliling());

		Lingkaran1 lingE = new Lingkaran1("4");
		System.out.println("Luas Lingkaran5 = "+lingE.luas());
		System.out.println("Keliling Lingkaran5 = "+lingE.keliling());

		Lingkaran1 lingF = new Lingkaran1(1, lingE);
		System.out.println("Luas Lingkaran6 = "+lingF.luas());
		System.out.println("Keliling Lingkaran6 = "+lingF.keliling());

		Lingkaran1 G = new Lingkaran1(lingE, 1);
		System.out.println("Luas Lingkaran7 = "+G.luas());
		System.out.println("Keliling Lingkaran7 = "+G.keliling());
	}
}
