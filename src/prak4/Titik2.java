package prak4;

public class Titik2 {
    int x;
    int y;

    public Titik2() {
        System.out.println("Konstruktor titik dijalankan!");
    }

    public Titik2(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("konstruktor titik 2 dijalankan!");
    }
    public Titik2(double x, int y) {
        this.x = (int) x;
        this.y = y;
        System.out.println("konstruktor titik 3 dijalankan!");
    }
    public Titik2(int x, double y) {
        this.x = x;
        this.y = (int) y;
        System.out.println("konstruktor titik 4 dijalankan!");
    }
    public Titik2(String x, int y) {
        this.x = Integer.parseInt(x);
        this.y = y;
        System.out.println("konstruktor titik 5 dijalankan!");
    }
    public Titik2(int x, String y) {
        this.x = x;
        this.y = Integer.parseInt(y);
        System.out.println("konstruktor titik 6 dijalankan!");
    }
    public Titik2(int x, int y, int z) {
        this.x = x;
        this.y = y;
        System.out.println("konstruktor titik 7 dijalankan!");
    }
    public int getTitikX() {
        return x;
    }
    public int getTitikY() {
        return y;
    }

}
