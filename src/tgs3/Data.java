package tgs3;
public class Data
{
	String nama;
	char jenisKelamin;
	int noTelp;

	public Data()	// konstruktor default
	{
		nama = "Joni";
		jenisKelamin = 'L';
		noTelp = 62877;
	}
	public Data(String nm, char jk, int no) // konstruktor copy
	{
		nama = nm;
		jenisKelamin = jk;
		noTelp = no;
	}
	public Data(Data se) // konstruktor copy objek
	{
		nama = se.nama;
		jenisKelamin = se.jenisKelamin;
		noTelp = se.noTelp;
	}
	public void cetak()
	{
		System.out.println("Nama : "+ nama);
		System.out.println("Jenis Kelamin : "+ jenisKelamin);
		System.out.println("No. Telp : "+ noTelp);
	}

	public static void main(String[]args)
	{
		Data se0 = new Data(); // memanggil konstruktor default
		se0.cetak();
		Data se1 = new Data("karla",'P',62888); // memanggil konstruktor copy
		se1.cetak();
		Data se2 = new Data("bambang",'L',62867); // memanggil konstruktor copy objek
		se2.cetak();

	}
}
