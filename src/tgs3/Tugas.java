package tgs3;
public class Tugas
{
	int x;
	int y;

	public int kali()
	{
		return(x*y);
	}
	public Tugas()	// konstruktor default
	{
		x = 10;
		y = 5;
	}
	public Tugas(int a, int b) // konstruktor copy
	{
		x = a;
		y = b;
	}
	public Tugas(Tugas se) // konstruktor copy objek
	{
		x = se.x;
		y = se.y;
	}

	public static void main(String[]args)
	{
		Tugas se0 = new Tugas(); // memanggil konstruktor default
		System.out.println(se0.kali());


		Tugas se1 = new Tugas(20, 30); // memanggil konstruktor copy
		System.out.println(se1.kali());

		Tugas se2 = new Tugas(se1); // memanggil konstruktor copy objek
		System.out.println(se2.kali());

	}

}
