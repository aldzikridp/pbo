package prak1;

class MobilClass {
    String model;
    String bensin;

    public void newMobil(String modelf, String bensinf){
        model=modelf;
        bensin=bensinf;
    }

    public void pakaiMobil(){
        bensin="kosong";
    }

    public void isiBensin(){
        bensin="penuh";
    }

    public void tampilMobil(){
        System.out.println("model mobil : "+model);
        System.out.println("bensin : "+bensin);
        System.out.println();
    }
}

public class Mobil {

   public static void main(String[] args){
       MobilClass baju1 = new MobilClass();
       baju1.newMobil("avanza","penuh");
       baju1.tampilMobil();
       baju1.pakaiMobil();
       baju1.tampilMobil();
       baju1.isiBensin();
       baju1.tampilMobil();
   }
}
