package prak3;

public class Tugas2 {

    public static void overload(int a, int b){
        System.out.println("dua parameter");
    }
    public static void overload(int a, int b, int c){
        System.out.println("tiga parameter");
    }

    public static void main(String[] args){
        Tugas2.overload(1,2);
        Tugas2.overload(1,2,3);
    }
}
