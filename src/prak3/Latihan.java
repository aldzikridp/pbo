package prak3;

class Calculator{
    public static void tambah(){
        System.out.println(0);
    }
    public static void tambah(double a){
        System.out.println(a*2);
    }
    public static void tambah(double a, double b){
        System.out.println(a+b);
    }
    public static void tambah(String a, String b){
        System.out.println(a+b);
    }
    public static void tambah(String a, int b){
        System.out.println(a+b);
    }
    public static void tambah(int a, int b){
        System.out.println(a+b+10);
    }
    public static void tambah(int a, int b, int c){
        System.out.println((a+b+c)*2);
    }
    public static void kali(){
        System.out.println("kosong");
    }
    public static void kali(double a, double b){
        System.out.println("double * double "+(a*b));
    }
    public static void kali(double a, int b){
        System.out.println("double * int "+(a*b));
    }
    public static void kali(int a, double b){
        System.out.println("int * double "+(a*b));
    }
    public static void kali(double a, double b, double c){
        System.out.println("double * double * double "+(a*b*c));
    }
    public static void kurang(){
        System.out.println(0);
    }
    public static void kurang(int a, int b){
        System.out.println("int - int  "+(a-b));
    }
    public static void kurang(int a, double b){
        System.out.println("int - double "+(a-b));
    }
    public static void kurang(double a, int b){
        System.out.println("double - int "+(a-b));
    }
    public static void kurang(int a, int b, int c){
        System.out.println("int - int - int "+(a-b-c));
    }
}
public class Latihan {

    public static void main(String[] args){
        Calculator operasi=new Calculator();
        operasi.tambah();
        operasi.tambah(0.4);
        operasi.tambah(0.4,0.9);
        operasi.tambah("akakom"," Yogyakarta");
        operasi.tambah("akakom",5);
        operasi.tambah(5,5);
        operasi.tambah(5,5,1);
        System.out.println();

        operasi.kali();
        operasi.kali(5.0,5.0);
        operasi.kali(5.0,1);
        operasi.kali(1,5.0);
        operasi.kali(0.5,0.5,0.5);
        System.out.println();

        operasi.kurang();
        operasi.kurang(2,1);
        operasi.kurang(3,1.0);
        operasi.kurang(3.0,1);
        operasi.kurang(14,6,3);
    }
}
